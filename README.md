# Intro Programming in Python

The Jupyter notebook in this repo has been used in a programming introduction course that was given in 2021 at Astron.

The notebook is licensed under the CC-BY license.
